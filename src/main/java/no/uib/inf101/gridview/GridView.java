package no.uib.inf101.gridview;

import java.awt.Dimension;
import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.GridDimension;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class GridView extends JPanel {
  
  private IColorGrid grid;
  private static final double OUTERMARGIN = 30;
  private static final Color OUTEROUTERCOLOR = Color.LIGHT_GRAY;
  private static final double INNERMARGIN = 30;

  public GridView(IColorGrid grid){
    this.setPreferredSize(new Dimension(400, 300)); //setPreferredSize er en metode i swing.JComponent.
    this.grid = grid; 
  }
  
  @Override
  public void paintComponent(Graphics g){
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
  }

  private void drawGrid (Graphics2D g2) {
    double outerWidth = this.getWidth() - 2 * OUTERMARGIN;
    double outerHeight = this.getHeight() - 2 * OUTERMARGIN;
    Rectangle2D box = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, outerWidth, outerHeight);
    g2.setColor(OUTEROUTERCOLOR);
    g2.fill(box);
    
    CellPositionToPixelConverter cellPositionConvert = new CellPositionToPixelConverter(box, grid, INNERMARGIN);
    drawCells(g2, grid, cellPositionConvert);
  }

  private static void drawCells (Graphics2D g2, CellColorCollection ccc, CellPositionToPixelConverter cellPositionConvert){
    for (CellColor cc: ccc.getCells()){
      Rectangle2D cell = cellPositionConvert.getBoundsForCell(cc.cellPosition());
      if (cc.color() == null){
        g2.setColor(Color.DARK_GRAY);
      }
      else {
        g2.setColor(cc.color());
      }
      g2.fill(cell);
    }
  }
}

