package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {

  private int rows;
  private int cols;
  private List<List<Color>> grid2;
  private String IndexOutOfBoundException;

  public ColorGrid (int rows, int cols){
    this.rows = rows;
    this.cols = cols;
    this.grid2 = new ArrayList<>();
    for (int i = 0; i < rows; i++){
      this.grid2.add(new ArrayList<Color>());
      for (int j = 0; j < cols; j++){
        this.grid2.get(i).add(null);
      }
    }
  }


  @Override
  public int rows() {
    return this.rows;
  }

  @Override
  public int cols() {
    return this.cols;
  }

  @Override
  public List<CellColor> getCells() {
    ArrayList<CellColor> gridCells = new ArrayList<>();
    for (int i = 0; i < grid2.size(); i++){
      for (int j = 0; j < grid2.get(0).size(); j++){
        //CellPosition pos = new CellPosition(i, j); //Lager ett nytt CellPositin object, som er nødvendig for å lage ett nytt CellColor objekt.
        //CellColor gridCellColor = new CellColor(pos, grid2.get(i).get(j)); //Lager et nytt CellColor objekt, er er det metoden krever at man returnerer en liste av.
        gridCells.add(new CellColor(new CellPosition(i, j), grid2.get(i).get(j)));//(gridCellColor); //Legger til hvert CellColor objekt, som består av en celle posisjon og farge.
      }
    } 
    return gridCells;
  }

  @Override
  public Color get(CellPosition pos) {
    int row = pos.row();
    int col = pos.col();

    if (this.grid2.size() > row && this.grid2.get(row).size() > col){
      return this.grid2.get(pos.row()).get(pos.col());
    }
    else if (this.grid2.size() < row && this.grid2.get(row).size() < col){
      throw new IndexOutOfBoundsException("Index for both row and col are out of bound");
    }
    else if (this.grid2.size() < row ){
      throw new IndexOutOfBoundsException("Index for row is out of bound");
    }
    else {
      throw new IndexOutOfBoundsException("Index for col is out of bound");
    }
    // basert på row og col i pos, slå opp på instansvariabelen som lagrer ting, finn riktig verdi
  }

  @Override
  public void set(CellPosition pos, Color color) {
    int row = pos.row();
    int col = pos.col();
    
    this.grid2.get(row).set(col, color);
    
    // lagre 'color' et sted basert på row, col 
  }
  // TODO: Implement this class
}
